import com.zuitt.example.*;

public class Main {

    public static void main(String[] args) {

        //OOP
        //OOP stands for Object-Oriented Programming
        //OOP is a programming model that allows developers to design software around data or objects rather than function and logic

        //OOP Concepts
        //Object - abstract idea that represents something in the real world
        //Example: the concept of a dog
        //Class - representation of the object using code
        //Example: Writing a code that would describe a dog
        //Instance - unique copy of the idea, made "physical"
        //actual creation or the physical copy
        //enables us to copy the concept such as an instance
        //Example: Instantiating a dog named Fluffy from the dog class

        //Objects
        //"States and Attributes" - what is the idea about?
        //Properties of the specific object
        //Dog's name, breed, color, etc.
        //Behaviors - what can the idea do?
        //What can the actual dog do?
        //bark, sit

        //Four Pillars of OOP
        //1. Encapsulation
        //a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
        //"data hiding" - the variables of a class will be hidden from the other classes, and can be accessed only through the methods of their current class
        //to achieve encapsulation, these are some examples:
        //variable/properties as private
        //provide a public setter and getter function

        //Create a car

        Car myCar = new Car();//we accessed the empty/default constructor
        myCar.drive();
        //assign the properties of myCar using setter methods
        myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2025);

        //myCar.name; (private)

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".");//My Toyota HiAce was made in 2023.

        //Composition and Inheritance

        //Inheritance - allows modelling an object that is a subset of another object
        //It defines "is a relationship"
        //Composition - allows modelling objects that are made up of other objects
        //both entities are dependent on each other
        //composed object cannot exist without the other entity
        //It defines "has a relationship"

        //example
        //A car is a vehicle - inheritance
        //A car has a driver - composition

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        myCar.setDriver("Dodong");

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        //2. Inheritance
        //can be defined as the process where one class acquire the properties and methods of another class
        //with the use of inheritance, the information is made manageable in hierarchical order

        Dog myPet = new Dog();
        myPet.setName("Bantay");
        myPet.setColor("Brown");
        myPet.speak();//Aww aww!
        System.out.println("My dog is " + myPet.getName() + " and it is a " + myPet.getColor() + " " + myPet.getBreed() + "!");

        myPet.call();

        //3. Abstraction - is a process where all logic and complexity are hidden from the user

        //Interfaces
        //this is used to achieve total abstraction
        //Creating abstract classes doesn't support "multiple inheritance" but it can be achieved with interfaces
        //interface allows multiple implementation, list functionalities
        //act as "contracts" wherein a class implements the interface, should have methods that the interfaces has defined in the class

        Person child = new Person();
        child.sleep();
        child.run();
        child.holidayGreet();
        child.morningGreet();

        //4.Polymorphism
            //this is usually done by function/method overloading

            //2 Main types of polymorphism
            //A. Static or compile-time polymorphism
                //methos with the same name, but they have diff data types and a diff number of arguments


        StaticPoly myAddition = new StaticPoly();

        //original method
        System.out.println(myAddition.addition(5, 6));
        //based on arguments
        System.out.println(myAddition.addition(5, 6, 7));
        //based on data types
        System.out.println(myAddition.addition(5.7, 6.6));

        Child myChild = new Child();
        myChild.speak();


            //B. Dynamic or run-time polymorphism
                //function is overridden by replacing the definition of the method in the parent class in the child class
        //System.out.println("Hello world!");




    }

}
