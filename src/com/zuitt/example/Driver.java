package com.zuitt.example;

public class Driver {
    //It is another component or composition that is needed by another class

    //properties
    private String name;
    //constructors
    public Driver(){}
    public Driver(String name){
        this.name = name;
    }
    //getter and setter
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
}

