package com.zuitt.example;

public class Person implements Actions, Greetings{

    public void sleep(){
        System.out.println("ZZZZZZZZZZ....(snore)");
    }

    public void run(){
        System.out.println("Runnnnning!!!");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }

    public void holidayGreet(){
        System.out.println("Happy Labor day!");
    }

    //Mini-Activity
    //Create a Greetings Interface morningGreet() and holidayGreet()
    //Implement it in Person.java



}

