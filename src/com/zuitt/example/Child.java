package com.zuitt.example;

public class Child extends Parent{

    public void speak(){
        super.speak();
        System.out.println("I am the child. I am no parent");
    }
}
