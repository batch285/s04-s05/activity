package com.zuitt.example;

public class StaticPoly {

    public int addition(int a, int b){
        return a + b;
    }

    public int addition(int a, int b, int c){
        return a + b+c;
    }

    public double addition(double a, double b){
        return a + b;
    }

}
