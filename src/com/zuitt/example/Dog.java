package com.zuitt.example;
//Child class of Animal class
//extends - is used to inherit the properties and methods of the parent class
public class Dog extends Animal{
    //properties
    private String breed;

    //constructor

    public Dog(){
        //super - direct access with the original constructor - to inherit
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    //getter and setter
    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }
    //method
    public void speak(){
        System.out.println("Aww aww!");
    }

    public void call(){
        //we have a direct access to the parent method
        super.call();
        System.out.println("Hi, I'm " + this.getName() + ". I am a dog! (from Dog.java)");
    }


}

